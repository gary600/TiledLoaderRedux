# TiledLoaderRedux
Load Tiled JSON map files on-the-fly from Godot!
This is very WIP, more work is coming.
## How to use
To use TiledLoaderRedux, copy its files somewhere within your project, and put this code wherever you want to load a map:
```gdscript
var TiledMap = preload("res://path/to/TiledMap.tscn")
var tmap = TiledMap.instance()
tmap.load_map("res://path/to/map.json")
add_child(tmap) # or put node somewhere else
```
## Features supported
- [x] JSON map/tileset files
- [ ] TMX/TSX map/tileset files (XML)
- [x] Basic map support (just tiles)
- [ ] Object layer support (as Area2D):
    - [x] Rectangles
    - [x] Points
    - [x] Polygons
    - [ ] Polylines
    - [ ] Object tiles
    - [ ] Ellipses
- [x] Image layers
- [x] Group layers