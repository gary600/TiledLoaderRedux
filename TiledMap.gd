extends Node2D

var tileset
var map_loaded

# Utility function to make things easier
func str_arr_slice(arr, from, to):
	var newarr = PoolStringArray()
	for i in range(from, to):
		newarr.append(arr[i])
	return newarr

# Tiled map loader
# Only supports Tiled JSON for now, with 1 image per tileset
func load_map(_filename):
	map_loaded = _filename
	var ext = _filename.split(".")[-1]

	if ext == "json":
		var file = File.new()
		file.open(_filename, File.READ)
		var text = file.get_as_text()
		var res = JSON.parse(text)
		file.close()
		if res.error > 0: # Unable to read file as JSON
			return ERR_FILE_UNRECOGNIZED

		# TODO: Ensure schema is correct
		var map = res.result
		var layers = map.layers
		var tilesets = map.tilesets

		# Create tileset (All tilesets are merged into one)
		self.tileset = TileSet.new()
		for tileset in tilesets:
			# Load tileset if external
			if tileset.has("source"):
				var fn_parts = _filename.split("/")
				var fn = str_arr_slice(fn_parts, 0, len(fn_parts)-1).join("/") + "/" + tileset.source
				file = File.new()
				file.open(fn, File.READ)
				text = file.get_as_text()
				res = JSON.parse(text)
				if res.error > 0:
					return ERR_FILE_UNRECOGNIZED
				var firstgid = tileset.firstgid
				tileset = res.result
				tileset["firstgid"] = firstgid
				file.close()

			# Load spritesheet as texture
			var fn_parts = _filename.split("/")
			var fn = str_arr_slice(fn_parts, 0, len(fn_parts)-1).join("/") + "/" + tileset.image
			var texture = load(fn)
			# Create tiles
			for id in range(tileset.tilecount):
				id = id + tileset.firstgid-1
				# Create tile
				self.tileset.create_tile(id)
				# Set tile texture
				self.tileset.tile_set_texture(id, texture)
				# Set tile region
				var x = int(id) % int(tileset.columns)
				var y = int(id / tileset.columns)
				var rect = Rect2(
					Vector2(
						(x*(tileset.tilewidth+tileset.spacing))+tileset.margin,
						(y*(tileset.tileheight+tileset.spacing))+tileset.margin
					),
					Vector2(tileset.tilewidth, tileset.tileheight)
				)
				self.tileset.tile_set_region(id, rect)

		# Create layers
		for layer in layers:
			# Call helper function
			var lyr = create_layer(layer, map)
			add_child(lyr)


# Function to create layers
# TODO: Support layer offsets
func create_layer(layer, map):
	# Tile layer
	if layer.type == "tilelayer":
		# Create layer
		var lyr = TileMap.new()
		# Set name if present
		if layer.name != "":
			lyr.set_name(layer.name)
		# Set tileset
		lyr.tile_set = tileset
		# Set cell size
		lyr.cell_size = Vector2(map.tilewidth, map.tileheight)
		# Copy tiles into layer
		for y in range(layer.height):
			for x in range(layer.width):
				lyr.set_cell(x, y, layer.data[y*layer.width+x]-1)
		return lyr

	# Object layer, implemented as Area2Ds
	# TODO: Possibly have an option to add custom nodes for sprites etc
	elif layer.type == "objectgroup":
		# Create grouping node
		var nd = Node2D.new()
		# Set name if present
		if layer.name != "":
			nd.set_name(layer.name)
		# Create objects
		for object in layer.objects:
			# Create area and collider
			var area = Area2D.new()
			var coll = CollisionShape2D.new()
			area.add_child(coll)
			# Set name if present
			if object.name != "":
				area.set_name(object.name)
			# Set position
			area.position = Vector2(object.x, object.y)

			# Create shape
			var shape
			if object.has("ellipse"):
				# TODO: Ellipse is not supported, approx. with polygon?
				# Temp 0-size rect
				shape = RectangleShape2D.new()
				shape.extents = Vector2(0, 0)

			elif object.has("polygon"):
				# TODO: Possibly calculate if polygon is concave/convex
				shape = ConvexPolygonShape2D.new()
				var points = PoolVector2Array()
				for point in object.polygon:
					points.append(Vector2(point.x, point.y))
				shape.set_point_cloud(points)

			elif object.has("polyline"):
				# TODO: Do something about polylines
				# Temp 0-size rect
				shape = RectangleShape2D.new()
				shape.extents = Vector2(0, 0)

			else: # Rectangle and point (0-size rectangle)
				shape = RectangleShape2D.new()
				shape.extents = Vector2(object.width, object.height)

			# TODO: Add support for tiles and text in object layers

			# Set shape
			coll.shape = shape
			# Add to group node
			nd.add_child(area)

		return nd

	# Group layers, just recursive call with grouping Node2D
	elif layer.type == "group":
		# Create grouping node
		var group = Node2D.new()
		# Set name if present
		if layer.name != "":
			group.set_name(layer.name)
		# Create sub-layers
		for lyr in layer.layers:
			group.add_child(create_layer(lyr))

		return group

	# Image layers, display as sprite
	elif layer.type == "imagelayer":
		# Create sprite
		var sprite = Sprite.new()
		# Set name if present
		if layer.name != "":
			sprite.set_name(layer.name)
		# Set image
		var fn_parts = map_loaded.split("/")
		var fn = str_arr_slice(fn_parts, 0, len(fn_parts)-1).join("/") + "/" + layer.image
		var texture = load(fn)
		sprite.texture = texture
		# Set position
		sprite.position = Vector2(layer.offsetx+texture.get_width()/2, layer.offsety+texture.get_height()/2)

		return sprite
